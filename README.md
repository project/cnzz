INTRODUCTION
------------

This is a small module for using [CNZZ] statistics on your site.

The reason why I create this module:  

If you create a custom block using the core module : block content. that block you created couldn't export.

This module using Drupal 8 Configuration api to export your settings. It's very useful.


REQUIREMENTS
------------

No special requirements.  


INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. See:
<https://drupal.org/documentation/install/modules-themes/modules-8>
for further information. 

2. This module would create two blocks. you just copy the statistic code from [CNZZ] , then paste that at each block setting page.


[CNZZ]: http://www.cnzz.com